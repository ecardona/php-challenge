<?php

namespace Tests;

use App\Call;
use App\Carrier;
use App\Mobile;
use App\Sms;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
	    $provider = new Carrier();
		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}

    /** @test */
	public function make_call_by_name()
    {
        $provider = new Carrier();
        $mobile = new Mobile($provider);
        /*$random_string_input = 'abcdefghijklmnopqrstuvwxyz';
        $random_string = substr(str_shuffle($random_string_input), 0, 10);*/
        $random_string = 'Edgar Cardona';
        $this->assertInstanceOf(Call::class,$mobile->makeCallByName($random_string));
    }

    /** @test */
    public function make_call_by_name_not_found()
    {
        $provider = new Carrier();
        $mobile = new Mobile($provider);
        $random_string = 'Ramos Alvin';
        $this->assertNull($mobile->makeCallByName($random_string));
    }

	/** @test */
	public function it_returns_null_when_number_empty()
	{
	    $provider = new Carrier();
		$mobile = new Mobile($provider);

		$this->assertNull($mobile->sendSms('',''));
	}

    /** @test */
	public function make_call_by_number()
    {
        $provider = new Carrier();
        $mobile = new Mobile($provider);
        /*$random_string_input = '0123456789';
        $random_string = substr(str_shuffle($random_string_input), 0, 10);*/
        $random_number = '960714280';
        $this->assertNotNull($mobile->sendSms($random_number,'Please send response.'));
    }

    /** @test */
    public function make_call_by_number_invalid()
    {
        $provider = new Carrier();
        $mobile = new Mobile($provider);
        $random_number = 'phonenumberx';
        $this->assertNull($mobile->sendSms($random_number,'Please send response.'));
    }

}
