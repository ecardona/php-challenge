<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	
	public static function findByName($name)
	{
        return $name === 'Edgar Cardona' ? new Contact() : null;
	}

	public static function findByNumber($number)
	{
        return $number !== '' ? new Contact() : null;
	}

	public static function validateNumber(string $number): bool
	{
		// logic to validate numbers
        return is_numeric($number);
	}
}