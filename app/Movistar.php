<?php


namespace App;


use App\Definitions\CarrierDefinition;

class Movistar extends CarrierDefinition
{
    public function __construct()
    {
        parent::__construct('Movistar');
    }

    public function dialContact(Contact $contact)
    {
        //
    }

    public function makeCall(): Call
    {
        //
    }

    public function sendSms(string $body): Sms
    {
        //
    }
}