<?php


namespace App;


use App\Definitions\CarrierDefinition;

class Claro extends CarrierDefinition
{
    public function __construct()
    {
        parent::__construct('Claro');
    }

    public function dialContact(Contact $contact)
    {
        //
    }

    public function makeCall(): Call
    {
        //
    }

    public function sendSms(string $body): Sms
    {
        //
    }
}