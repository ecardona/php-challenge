<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;


class Mobile
{

	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		$contact = ContactService::findByName($name);

		if($contact === null) return $contact;

		$this->provider->dialContact($contact);

		return $this->provider->makeCall();
	}

	public function sendSms($number,$body)
    {
        $contact = ContactService::findByNumber($number);

        if($contact === null) return $contact;

        return ContactService::validateNumber($number) ? $this->provider->sendSms($body) : null;
    }


}
