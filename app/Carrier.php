<?php
namespace App;

use App\Interfaces\CarrierInterface;

class Carrier implements CarrierInterface
{
    public function dialContact(Contact $contact)
    {
        //
    }

    public function makeCall(): Call
    {
        return new Call();
    }

    public function sendSms(string $body): Sms
    {
        return new Sms($body);
    }
}